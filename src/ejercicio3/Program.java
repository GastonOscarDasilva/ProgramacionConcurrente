package ejercicio3;

public class Program {

	public static void main(String[] args) {
		Buffer buffer = new Buffer(2);
		//System.out.println(buffer.next(10));
		Productor p = new Productor(buffer); 
		Consumidor c = new Consumidor(buffer);
		
		p.start();
		c.start();

	}
}
