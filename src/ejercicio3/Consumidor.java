package ejercicio3;

public class Consumidor extends Thread{
	private final Buffer buffer;
	
	public Consumidor(Buffer b){
		this.buffer = b;
	}
	
	public void run(){
		while(true){
			System.out.println(this.buffer.sacar());
		}
	}
}
