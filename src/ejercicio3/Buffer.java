package ejercicio3;

import java.util.ArrayList;

public class Buffer {
	private ArrayList<Integer> bufferData ;
	private int end = 0;
	private int begin = 0;
	private int n; 
	
	public Buffer(int i){
		this.bufferData = new ArrayList<Integer>(i);
		this.n =i;
		
	}
	
	public int next(int i){
		return (i+1)%(this.n +1);
	}
	
	public boolean isFull(){
		return  this.next(begin) == end;
		
	}
	
	public boolean isEmpty(){
		return this.begin == this.end;
	}
	
	public synchronized void poner(int valor){
		while(this.isFull()){
			try {
				System.out.println("entro al wait del Poner");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.bufferData.add(begin, valor);
		this.begin = this.next(this.begin);
		notifyAll();
	}
	
	public synchronized int sacar(){
		while(this.isEmpty()){
				try {
					System.out.println("entro al wait del sacar");
					wait();
					} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			}
			 int res = this.bufferData.get(this.end);
			this.end = this.next(this.end);
			notifyAll();
			return res;
	}
}
