package ejercicio3;

public class Productor extends Thread {
	private Buffer buffer;
	
	public Productor(Buffer b){
		this.buffer = b;
	}
	
	public void run(){
		int i = 0;
		while(true){
			this.buffer.poner(i);
			i++;
		}
	}
}
