package ejercicio6;

public class Grid {
	private int consumidores = 0; 
	private int productores = 0;
	private int n ;
	private int waitingProductoresASalir= 0;
	
	public Grid(int nMasQueConsumidores){
		n = nMasQueConsumidores;
	}
	public synchronized void  startProducir(){
		
		while((productores - consumidores)>= n){ // se supone que siempre hay mas productores
			try {
				System.out.println("Productor entro al wait del StartProducir");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		productores++;
		System.out.println("Productor entro");
		notifyAll();
	}
	
	public synchronized void  stopProducir(){
		while(consumidores == productores){// algun consumidor se queda sin suministro si este productor para
				try {
					waitingProductoresASalir++; 
					System.out.println("Entro al wait del stopPruducir");
					wait();// espera a que los  productores sean mayores
				} catch (InterruptedException e) {e.printStackTrace();}
				 finally { waitingProductoresASalir --; }

		}
		System.out.println("Un Productor dejo de producir");	
		productores--;
		
	}
	public synchronized void startConsumir(){
		
		while((consumidores > productores)|| waitingProductoresASalir != 0){// cantidad igual o mayor a productores
			try {
				System.out.println("Entro al wait de consumir");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		consumidores++;
		System.out.println("consumidor entro");
		System.out.println("consumidores:"+this.consumidores + "productores:"+this.productores);
		notifyAll();
		//Puede consumir 
	}
	
	public synchronized void stopConsumir(){
		consumidores--;
	}
	
	
}
