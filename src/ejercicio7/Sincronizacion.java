package ejercicio7;

public class Sincronizacion {
	
	private int cantidadPizzasGrandes = 0;
	private int cantidadPizzasPequenhas = 0;
	
	public synchronized void colocarPizzaGrande(){
		cantidadPizzasGrandes++;
		notifyAll();
	}
	
	public synchronized  void colocarPizzaPequenhas(){
		cantidadPizzasPequenhas++;
		notifyAll();
	}
	public synchronized void tomarPizzaGrande(){
	    // esto es un mensaje solo, cambialo boludo no te olvides
		while(cantidadPizzasGrandes == 0){
			System.out.println("wait del TomarPizzaGrande");
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// PREGUNTAR COMO HACER QUE TOMEN DOS PIZZAS PEQUEAS SI NO HAY GRANDES
		cantidadPizzasGrandes--;
		
	}
	
	public synchronized void tomarPizzaPequenha(){
		while(cantidadPizzasPequenhas == 0){
			try {
				System.out.println("wait del TomarPizzaPequenha");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		cantidadPizzasPequenhas--;
	}
}
