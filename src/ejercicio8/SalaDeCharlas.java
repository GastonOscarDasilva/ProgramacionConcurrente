package ejercicio8;

public class SalaDeCharlas {
	
	private int contadorDePersonasPorCharla = 0;
	private boolean ComienzoDeCharla = false;
	
	public synchronized void OyentesAsistio(){
		while(ComienzoDeCharla || contadorDePersonasPorCharla == 50){
			try {
				System.out.println("Esperando en el wait de OyentesAsistio");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		contadorDePersonasPorCharla++;
		System.out.println("CantPersonasEnLaSala:"+ this.contadorDePersonasPorCharla);
		notify();
		// Esperar a que la CHARLA COMIENZE
		
		
	}
	
	public synchronized void CharlaComienza(){
		//descansa 5min si no hay oyentes
		while(contadorDePersonasPorCharla <40){
			try {
				wait(); System.out.println("Esperando a que sean  por lo menos 40 oyentes");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Charla Comenzo");
		ComienzoDeCharla = true;
		notifyAll();
	}
	
	public synchronized void CharlaFinaliza (){
		System.out.println("Charla Finalizo");
		ComienzoDeCharla = false;
		contadorDePersonasPorCharla = 0;// los oyentes se van 
		notifyAll();
	}
}
