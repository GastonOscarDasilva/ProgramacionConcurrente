package ejercicio8;


public class Orador extends Thread {
	private final SalaDeCharlas sala;
	
	public Orador(SalaDeCharlas sala){
		this.sala = sala;
	}
	
	public void run(){
		while(true){
			this.sala.CharlaComienza(); // Comienza la charla cuando el orador llego
			try {
				sleep(355);// Tiempo que tarda en dar la charla
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.sala.CharlaFinaliza();
			try {
				sleep(75);// Tiempo que se toma el orador entre charla
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
}
