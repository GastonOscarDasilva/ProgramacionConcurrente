package ejercicio8;


public class ProgramEjercicio8 {
	public static void main(String[] args) {
		SalaDeCharlas sala = new SalaDeCharlas();
		Orador or = new Orador(sala);
		Asistente as = new  Asistente(sala);
		
		or.start();
		as.start();
	}
}
