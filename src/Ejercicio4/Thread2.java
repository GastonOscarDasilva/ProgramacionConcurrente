package Ejercicio4;


public class Thread2 extends Thread{
	private final SecuenciadorTernario sec;
	
	public Thread2(SecuenciadorTernario sec){
		this.sec = sec;
	}
	
	public void run(){
		while(true){
			this.sec.segundo();
		}
	}
}