package Ejercicio4;

public class SecuenciadorTernario {
	
	private int secuenciador = 0;
	
	public int  nextTurno(){
		secuenciador = (secuenciador+1) % 3;
		return secuenciador;
	}
	public synchronized void primero(){
		int suTurno = 0;
		while(secuenciador != suTurno ){
			try {
				System.out.println("entro al wait de primero");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Primero");
		this.nextTurno();
		notifyAll();
	}
	public synchronized void segundo(){
		int suTurno = 1;
		while(secuenciador != suTurno ){
			try {
				System.out.println("entro al wait de segundo");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Segundo");
		this.nextTurno();
		notifyAll();
	}
	public synchronized void tercero(){
		int suTurno = 2;
		while(secuenciador != suTurno ){
			try {
				System.out.println("entro al wait de tercero");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Tercero");
		this.nextTurno();
		notifyAll();
	}
}

